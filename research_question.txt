Group: Group 74
    
    Question
    ========
    
    RQ: Is there any correlation between life expectancy and alcohol consumption in France between 2000 to 2015?
    
    Null hypothesis: There is no correlation between life expectancy and alcohol consumption in France between 2000 to 2015.
    
    Alternative hypothesis: There is a correlation between life expectancy and alcohol consumption in France between 2000 to 2015.
    
    Dataset
    =======
    
    URL: https://www.kaggle.com/kumarajarshi/life-expectancy-who


    Column Headings:

    ```
	
    > LIFE <- read.csv("Life ExpectancyData.csv")
    > colnames(LIFE)
   [1] "Country"                         "Year"                           
   [3] "Status"                          "Life.expectancy"                
   [5] "Adult.Mortality"                 "infant.deaths"                  
   [7] "Consumption"                     "percentage.expenditure"         
   [9] "Total.expenditure"               "GDP"                            
  [11] "Population"                      "Income.composition.of.resources"
   
   ```